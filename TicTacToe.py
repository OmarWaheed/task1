from pip._vendor.distlib.compat import raw_input


land = [0, 1, 2, 3, 4, 5, 6, 7, 8]


def Show():
    print(land[0], "|", land[1], "|", land[2])
    print("----------")
    print(land[3], "|", land[4], "|", land[5])
    print("----------")
    print(land[6], "|", land[7], "|", land[8])


def ChoosePlayer():
    player=raw_input("Choose First Player X or O : ")
    return player


def SelectArea():
    area=raw_input("Select Area #: ")
    area=int(area)
    return area


def ChangeAreas(area, player):
    land[area] = player


def gamestart():
    p = ChoosePlayer()
    flag=0
    while CheckWinner() != 'X' and CheckWinner() != 'O' and flag != 9:
         Show()
         if flag % 2 != 0:
            p = "O"
         else:
            p = "X"
         flag = flag + 1
         a = SelectArea()
         ChangeAreas(a, p)

    print("Winner Is :", CheckWinner())



def CheckWinner():
    if (land[0] == "X" and land[1] == "X" and land[2] == "X"
     or land[3] == "X" and land[4] == "X" and land[5] == "X"
     or land[6] == "X" and land[7] == "X" and land[8] == "X"
     or land[0] == "X" and land[3] == "X" and land[6] == "X"
     or land[1] == "X" and land[4] == "X" and land[7] == "X"
     or land[2] == "X" and land[5] == "X" and land[8] == "X"
     or land[0] == "X" and land[4] == "X" and land[8] == "X"
     or land[2] == "X" and land[4] == "X" and land[6] == "X"):
        return 'X'
    elif(   land[0] == "O" and land[1] == "O" and land[2] == "O"
         or land[3] == "O" and land[4] == "O" and land[5] == "O"
         or land[6] == "O" and land[7] == "O" and land[8] == "O"
         or land[0] == "O" and land[3] == "O" and land[6] == "O"
         or land[1] == "O" and land[4] == "O" and land[7] == "O"
         or land[2] == "O" and land[5] == "O" and land[8] == "O"
         or land[0] == "O" and land[4] == "O" and land[8] == "O"
         or land[2] == "O" and land[4] == "O" and land[6] == "O"):
        return 'O'


gamestart()

